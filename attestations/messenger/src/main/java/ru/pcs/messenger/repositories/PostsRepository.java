package ru.pcs.messenger.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.messenger.models.Post;

import java.util.List;

public interface PostsRepository extends JpaRepository<Post, Long> {
    List<Post> findAllByAuthor_Id(Long authorId);
}
