package ru.pcs.messenger.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.pcs.messenger.models.Post;
import ru.pcs.messenger.repositories.PostsRepository;

import java.util.List;

@Controller
public class AuthorsController {

    @Autowired
    private PostsRepository postsRepository;

    @GetMapping("/authors/{author-id}")
    public String getPostsOfAuthor(@PathVariable("author-id") Long authorId, Model model) {
        List<Post> posts = postsRepository.findAllByAuthor_Id(authorId);
        model.addAttribute("posts", posts);
        return "author_posts";
    }
}
